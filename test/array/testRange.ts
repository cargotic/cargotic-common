import { range } from "../../src";

test("array range generation", () => {
  expect(range(0, 1)).toStrictEqual([0]);
  expect(range(0, 10)).toStrictEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
});
