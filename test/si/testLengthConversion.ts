import { LengthUnit, LengthUnitConversion, convert } from "../../src";

test("test length unit conversion", () => {
  expect(convert(1, LengthUnit.M, LengthUnit.M, LengthUnitConversion)).toBe(1);
  expect(
    convert(1, LengthUnit.M, LengthUnit.CM, LengthUnitConversion)
  ).toBe(100);

  expect(
    convert(1, LengthUnit.CM, LengthUnit.CM, LengthUnitConversion)
  ).toBe(1);

  expect(
    convert(1, LengthUnit.CM, LengthUnit.M, LengthUnitConversion)
  ).toBe(0.01);
});
