const loadScript = (url: string) =>
  new Promise((resolve) => {
    const element = document.createElement("script");

    element.setAttribute("src", url);
    element.addEventListener("load", resolve);

    document.body.append(element);
  });

export default loadScript;
