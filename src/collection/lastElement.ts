const lastElement = <T>(collection: T[]) =>
  collection[collection.length - 1];

export default lastElement;
