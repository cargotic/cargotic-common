const isEmpty = (collection: any[] | object) =>
  Object.keys(collection).length === 0;

export default isEmpty;
