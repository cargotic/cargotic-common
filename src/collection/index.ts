export { default as firstElement } from "./firstElement";
export { default as isEmpty } from "./isEmpty";
export { default as lastElement } from "./lastElement";
