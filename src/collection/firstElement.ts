const firstElement = <T>(collection: T[]) =>
  collection[0];

export default firstElement;
