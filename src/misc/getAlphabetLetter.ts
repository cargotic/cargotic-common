const ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

const getAlphabetLetter = (index: number) =>
  ALPHABET[index % ALPHABET.length];

export default getAlphabetLetter;
