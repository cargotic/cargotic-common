import serializeQueryParameters from "./serializeQueryParameters";

const appendQueryParameters = (url: string, parameters: object) =>
  `${url}?${serializeQueryParameters(parameters)}`;

export default appendQueryParameters;
