const serializeQueryParameters = (parameters: object) =>
  Object
    .entries(parameters)
    .filter(([, value]) => value)
    .map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
    .join("&");

export default serializeQueryParameters;
