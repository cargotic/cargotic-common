const removeIndex = <T>(array: T[], index: number) => (
  array.filter((_, other) => other !== index)
);

export default removeIndex;
