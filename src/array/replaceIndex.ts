const replaceIndex = <T>(array: T[], index: number, element: T) => (
  Object.assign([], array, { [index]: element }) as T[]
);

export default replaceIndex;
