import NoSuchElementError from "./NoSuchElementError";

const findOrThrow = <T>(
  array: T[],
  predicate: (element: T) => boolean,
  error: () => Error = () => new NoSuchElementError()
) => {
  const element = array.find(predicate);

  if (!element) {
    throw error();
  }

  return element;
};

export default findOrThrow;
