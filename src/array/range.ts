const range = (from: number, to: number) => (
  new Array(to - from)
    .fill(null)
    .map((_, index) => from + index)
);

export default range;
