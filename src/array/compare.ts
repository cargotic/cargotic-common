const compare = <T>(a: T[], b: T[]) => {
  if (a.length !== b.length) {
    return false;
  }

  return a.every((x, index) => x === b[index]);
};

export default compare;
