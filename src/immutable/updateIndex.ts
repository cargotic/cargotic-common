const updateIndex = <T>(array: T[], index: number, element: T) =>
  [...array.slice(0, index), element, ...array.slice(index + 1)];

export default updateIndex;
