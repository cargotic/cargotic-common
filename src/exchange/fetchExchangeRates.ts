import Axios from "axios";

import { Currency, Conversion } from "../si";
import { appendQueryParameters } from "../url";

const EXCHANGE_RATES_API_URL = "https://api.exchangeratesapi.host/latest";

const fetchExchangeRates = async (base = Currency.CZK) => {
  const url = appendQueryParameters(EXCHANGE_RATES_API_URL, {
    base,
    source: "ecb "
  });
  const { data } = await Axios.get<Conversion<Currency>>(url);

  return data;
};

export default fetchExchangeRates;
