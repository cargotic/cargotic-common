import Conversion from "./Conversion";

const convert = <T extends string>(
  magnitude: number,
  source: T,
  target: T,
  { base, rates }: Conversion<T>
) => {
  if (source === target) {
    return magnitude;
  }

  if (source === base) {
    return magnitude * rates[target];
  }

  if (target === base) {
    return magnitude / rates[source];
  }

  return (magnitude / rates[source]) * rates[target];
};

export default convert;
