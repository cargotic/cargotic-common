import LengthUnit from "./LengthUnit";

const LengthUnitConversion = {
  base: LengthUnit.M,
  rates: {
    [LengthUnit.M]: 1,
    [LengthUnit.CM]: 100
  }
};

export default LengthUnitConversion;
