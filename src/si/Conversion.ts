interface Conversion<T extends string> {
  base: T,
  rates: {
    [key in T]: number
  }
}

export default Conversion;
