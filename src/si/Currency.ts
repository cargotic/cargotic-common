enum Currency {
  CZK = "CZK",
  EUR = "EUR"
}

export default Currency;
