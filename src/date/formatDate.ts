import { format } from "date-fns";

const DATE_FORMAT = "dd.MM";
const PRECISE_DATE_FORMAT = `${DATE_FORMAT}. yyyy`;

const formatDate = (date: Date, precise?: boolean) => (
  format(date, precise ? PRECISE_DATE_FORMAT : DATE_FORMAT)
);

export default formatDate;
