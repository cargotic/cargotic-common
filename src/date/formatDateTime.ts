import formatDate from "./formatDate";
import formatTime from "./formatTime";

const formatDateTime = (date: Date) => (
  `${formatDate(date)} ${formatTime(date)}`
);

export default formatDateTime;
