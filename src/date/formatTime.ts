import { format } from "date-fns";

const TIME_FORMAT = "HH:mm";

const formatTime = (date: Date) => (
  format(date, TIME_FORMAT)
);

export default formatTime;
