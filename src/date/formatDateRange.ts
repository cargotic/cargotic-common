import { isSameDay } from "date-fns";

import formatDateTime from "./formatDateTime";
import formatTime from "./formatTime";

const formatDateRange = (from: Date, to?: Date) => {
  const formattedFrom = formatDateTime(from);

  if (!to) {
    return formattedFrom;
  }

  const format = isSameDay(from, to)
    ? formatTime
    : formatDateTime;

  return `${formattedFrom} - ${format(to)}`;
};

export default formatDateRange;
